#! /bin/bash
set -e
cd ${PROJECT_PATH}
echo "Downloading dependencies..."
dotnet restore
echo "Dependencies downloaded"
echo "Building Server side application..."
dotnet build **/project.json
echo "Server side application built"
echo "Testing server side application..."
dotnet test test/**/project.json
echo "Building client-side appliction..."
cd ${CLIENT_APPLICATION_SOURCE_PATH}
npm i
npm run ${NPM_RUN_CONFIG}
echo "Client side application built"
echo "Publishing application..."
cd ${SERVER_APPLICATION_SOURCE_PATH}
dotnet publish -o ${PUBLISH_PATH}
echo "Application published"